@extends('layouts.app')

@section('content')
    <form action="" method="POST">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-hover">
                                <div class="form-group">
                                    <label>Введіть послилання :</label>
                                    <input type="url" name="url" class="form-control" placeholder="Введіть лінк"
                                           required="">
                                </div>
                                <div class="form-group">
                                    <label>Введіть кількість сторінок :</label>
                                    <input type="number" name="page" class="form-control"
                                           placeholder="Введіть кількість сторінок" required="">
                                </div>
                                {{ csrf_field() }}
                                <div class="form-group">
                                    <button class="btn btn-success btn-Łsubmit">Відправити</button>
                                </div>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection