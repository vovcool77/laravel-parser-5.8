@extends('layouts.app')

@section('content')
    @php /** @var \App\Models\Product $product */ @endphp
    <form method="POST" action="{{ route('categories.update', $product->p_id) }}">
        @method('PATCH')
        @csrf
        <div class="container" id="edit_product">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    @php
                   /** @var Illuminate\Support\ViewErrorBag $errors */
                    @endphp
                    {{--Editing of category--}}
                    @include('categories.includes.result_messages')
                    <div class="row justify-content-center">
                        <div class="col-md-9">
                            @include('categories.includes.item_edit_main_col')
                        </div>
                        <div class="col-md-3">
                            @include('categories.includes.item_edit_add_col')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection