<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;


class Parser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'parser:start {from?}{count?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'parser:start
                        {from : Елемент парсингу}
                        {count : Кількість сторінок}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(\App\Models\Parser $parser)
    {
        $url = Config::get('settings.url_to_parse');
        $page = Config::get('settings.number_of_pages') ;

        $parser->parserStart($url, $page);

        echo 'Mission complete :)';
    }
}
