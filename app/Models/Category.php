<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Category
 *
 * @package App\Models
 *
 * @property string                    $c_id
 * @property string                    $c_name
 */
class Category extends Model
{
    protected $primaryKey = 'c_id';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable
        = [
            'c_id',
            'c_name',
        ];


    /**
     * Category & Product relations.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function product()
    {
        return $this->belongsToMany(Product::class, 'product_categories', 'category_id', 'product_id');
    }

    /**
     * Saving category into DB.
     * @param $inputCategory
     */
    public function saveCategories($inputCategory)
    {
        foreach ($inputCategory as $category => $categoryId) {
            $cat = new Category();
            $cat->c_id = $categoryId;
            $cat->c_name = $category;
            $cat->save();
        }
    }

    /**
     * Get category from DB.
     * @param $id
     * @param $number
     * @return mixed
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getCategory($id, $number)
    {
        $result = Category::find($id)->product()->paginate($number);

        return $result;
    }
}


