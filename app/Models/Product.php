<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Product
 *
 * @package App\Models
 *
 * @property string                   $p_id
 * @property string                   $p_name
 * @property string                   $description
 * @property string                   $specification
 * @property integer                  $price
 */
class Product extends Model
{

    protected $primaryKey = 'p_id';
    public $timestamps = false;
    public $incrementing = false;

    protected $fillable
        = [
            'p_id',
            'p_name',
            'description',
            'specification',
            'price',
        ];


    /** Product & Image relations.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function image()
    {
       return $this->hasMany(Image::class, 'product_id', 'p_id')->select('img_url');
    }

    /** Product & Category relations.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function category()
    {
       return $this->belongsToMany(Category::class, 'product_categories', 'product_id', 'category_id');
    }

    /**
     * Saving products into DB.
     * @param $inputProducts
     * @param $categories
     */
    public function saveProdcuts($inputProducts, $categories)
    {
        foreach ($inputProducts as $product) {
            $prd = new Product();
            $prd->p_id = $product['id'];
            $prd->p_name = $product['title'];
            $prd->description = $product['description'];
            $prd->specification = $product['specification'];
            $prd->price = $product['price'];
            $prd->save();


            foreach ($product['images'] as $image){
                $img = new Image();
                $img->product_id = $product['id'];
                $img->img_url = $image;
                $img->save();
            }

            foreach ($product['categories'] as $categoryName => $productId){
                $product_categories = new ProductCategory();
                $product_categories->product_id = $productId;
                $product_categories->category_id = $categories[$categoryName];
                $product_categories->save();

            }
        }
    }

    /**
     * Get products from DB.
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getProduct($id)
    {
        $product = Product::find($id);

        return $product;
    }

    /**
     * Get images from DB.
     * @param $id
     * @return \Illuminate\Support\Collection
     */
    public function getImage($id)
    {
        $result = Product::find($id)->image()->get();

        return $result;
    }

    /**
     * Updating product.
     *
     * @param $id
     * @param $p_name
     * @param $description
     * @param $specification
     * @param $price
     * @return int
     */
    public function updateProduct($id, $p_name, $description, $specification, $price)
    {
        $product = Product::where('p_id', $id)->update([
            'p_name' => $p_name,
            'description' => $description,
            'specification' => $specification,
            'price' => $price,
        ]);

        return $product;
    }

    /**
     * Deleting product.
     *
     * @param $id
     * @return int
     */
    public function productDelete($id)
    {
        $result = Product::where('p_id', $id)->delete();

        return $result;
    }
}

