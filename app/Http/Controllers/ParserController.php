<?php

namespace App\Http\Controllers;

use App\Models\Parser;
use Illuminate\Http\Request;


class ParserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('add');
    }

    /**
     * @param Request $request
     * @param Parser $parser
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function add(Request $request, Parser $parser)
    {
        $parser->parserStart(
            $request->url,
            $request->page
        );

        return redirect('categories');
    }
}
