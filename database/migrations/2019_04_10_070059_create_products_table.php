<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {

            $table->string('p_id', 64)->index();

            $table->string('p_name');
            $table->text('description')->nullable();

            $table->text('specification')->nullable();

            $table->bigInteger('price');

        });
    }

    public function down()
    {
        Schema::table('products', function (Blueprint $table){

            $table->dropIndex('p_id');

            $table->dropColumn('p_name');
            $table->dropColumn('description');

            $table->dropColumn('specification');

            $table->dropColumn('price');

        });
    }
}
